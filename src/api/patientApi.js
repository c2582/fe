import config from "./apiConfig"
import axios from "axios"

const getAllPatient = (dateTime="",area="",callback)=>{
    axios.post("http://localhost:5000/getPatient",{dateTime:dateTime,area:area},config).then((res)=>{
        console.log("getAllPatient",res);
        callback(res.data)
    }).catch((err)=>{
        console.log(err);
        callback(null)
    })
}

const getPatientGroupArea = (dateTime="",callback)=>{
    axios.post("http://localhost:5000/getPatientGroupArea",{dateTime:dateTime},config).then((res)=>{
        console.log("getPatientGroupArea",res);
        callback(res.data)
    }).catch((err)=>{
        console.log(err);
        callback([])
    })
}

const getPatientStatus = (dateTime="",callback)=>{
    axios.post("http://localhost:5000/getPatientStatus",{dateTime:dateTime},config).then((res)=>{
        console.log("getPatientStatus",res);
        callback(res.data)
    }).catch((err)=>{
        console.log(err);
        callback([])
    })
}
const getRegionStatusMap = (dateTime="",callback)=>{
    axios.post("http://localhost:5000/getRegionStatusMap",{dateTime:dateTime},config).then((res)=>{
        console.log("getRegionStatusMap",res);
        callback(res.data)
    }).catch((err)=>{
        console.log(err);
        callback([])
    })
}
const getDistrictStatusMap = (dateTime="",callback)=>{
    axios.post("http://localhost:5000/getDistrictStatusMap",{dateTime:dateTime},config).then((res)=>{
        console.log("getDistrictStatusMap",res);
        callback(res.data)
    }).catch((err)=>{
        console.log(err);
        callback([])
    })
}
export {getAllPatient,getPatientGroupArea,getPatientStatus,getDistrictStatusMap,getRegionStatusMap}