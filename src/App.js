import React from 'react';
import './App.css';
import { render } from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import Table from './pages/Table/Table';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/"  element={<Table/>} />
        <Route path="/table" exact element={Table} />
      </Routes>

    </BrowserRouter>
  );
}

export default App;