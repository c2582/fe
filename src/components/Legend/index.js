
import { Paper } from '@mui/material'
import React from 'react'
import LegendPonit from './LegendPonit'
import LevelChecBox from './LevelChecBox'

function Legend() {
    let data = [
        {color : "#3BD248", label:"CAP_1"},
        {color : "#D4D727", label:"CAP_2"},
        {color : "#F21115", label:"CAP_3"},
        {color : "#9F0D95", label:"CAP_4"}
    ]
    return (
        <Paper style={{ width:200, height:300,padding:10, position:"fixed", bottom:120,right:10, background:"white",zIndex:10}}>
            <h3>Xem theo khu vực</h3>
            <LevelChecBox/>
            <h3>Phân bố cấp độ</h3>
            {data.map((pt)=><LegendPonit color={pt.color} label={pt.label}/>)}
            
        </Paper>
    )
}

export default Legend
