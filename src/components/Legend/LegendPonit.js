import React from 'react'

function LegendPonit({color,label}) {
    return (
        <div style={{width:"100%",margin:5, display:"flex"}}>
            <div style={{width:"20px",height:20,background:`${color}`}}></div>
            <div style={{textAlign:"left",verticalAlign:"center", width:`calc(100% - 50px)`,marginLeft:10}}>{label}</div>
        </div>
    )
}

export default LegendPonit
