import { Checkbox } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {setRegionStatus,setDistrictStatus} from "../../feature/MapStatusSlice"
function LevelChecBox() {
    const dispatch = useDispatch()
    const [DistrictLevel, setDistrictLevel] = React.useState(false)
    const [RegionLevel, setRegionLevel] = React.useState(false)
    const handleCheckDistrict = (e)=>{
        setDistrictLevel(e.target.checked )
        dispatch(setDistrictStatus(e.target.checked))
    }
    const handleCheckRegion = (e)=>{
        setRegionLevel(e.target.checked )
        dispatch(setRegionStatus(e.target.checked))
    }
    return (
        <>
            <div style={{width:"100%",margin:5, display:"flex"}}>
                <div style={{width:"20px",height:20 }}><Checkbox checked={RegionLevel} onChange={handleCheckRegion}/></div>
                <div style={{textAlign:"left",verticalAlign:"center", width:`calc(100% - 50px)`,marginLeft:20,paddingTop:10}}>Quận Huyện</div>
            </div>
            <div style={{width:"100%",margin:5, display:"flex"}}>
                <div style={{width:"20px",height:20 }}><Checkbox checked={DistrictLevel} onChange={handleCheckDistrict}/></div>
                <div style={{textAlign:"left",verticalAlign:"center", width:`calc(100% - 50px)`,marginLeft:20,paddingTop:10}}>Phường Xã</div>
            </div>
        </>
    )
}

export default LevelChecBox
