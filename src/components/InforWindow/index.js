import { Typography } from '@mui/material'
import React from 'react'
import { InfoWindow } from "@react-google-maps/api";
import parse from 'html-react-parser'
function CustomInforWindow({position,content}) {
    return (
        <InfoWindow position={position}>
            <div
                style={{background:"white"}}>
                    <Typography variant="overline" noWrap={false} display="block" gutterBottom>
                        {parse(content)}
                    </Typography>
                </div>
        </InfoWindow>
    )
}

export default CustomInforWindow
