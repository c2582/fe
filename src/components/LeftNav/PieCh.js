import { PieChart, Pie, Cell, Legend } from "recharts";
import React, { useEffect } from 'react'


const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index
}: any) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text
            x={x}
            y={y}
            fill="white"
            textAnchor={x > cx ? "start" : "end"}
            dominantBaseline="central"
        >
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
};
const getRandomColor = () => {
    const randomColor = Math.floor(Math.random()*16777215).toString(16);
    return "#"+randomColor;
  }
function PieCh({ name, data=[] }) {
    const [Data, setData] = React.useState([
        { name: "Group A", value: 400 },
        { name: "Group B", value: 300 },
        { name: "Group C", value: 300 },
        { name: "Group D", value: 200 }])
    useEffect(() => {
        setData(data)
        return () => {

        }
    }, [data])
    return (
        <div>
            <h4>{name}</h4>

            <PieChart width={200} height={300}>
                <Pie
                    data={Data}
                    cx={100}
                    cy={100}
                    labelLine={false}
                    label={renderCustomizedLabel}
                    outerRadius={90}
                    fill="#8884d8"
                    animationBegin
                    dataKey="value"
                >
                    {Data.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={getRandomColor()} />
                    ))}
                </Pie>
                <Legend />
            </PieChart>
        </div>

    )
}

export default PieCh
