import React, { useEffect } from 'react'
import { Grid, Paper, TextField, Typography } from "@mui/material"
import LineCh from './LineCh';
import PieCh from './PieCh';
import {useDispatch} from "react-redux"
import {setqueryDateTime} from "../../feature/MapStatusSlice"
import {getAllPatient,getPatientGroupArea,getPatientStatus} from "../../api/patientApi"
function LeftNav({ Name = "Thông tin toàn Đà Nẵng", area }) {
    const dispatch = useDispatch()
    const [value, setValue] = React.useState(new Date('2014-08-18T21:11:54').toISOString());
    const handleChange = (newValue) => {
        console.log(newValue);
        dispatch(setqueryDateTime(newValue))
        setValue(newValue);
    };
    const [DieData, setDieData] = React.useState([])
    const [PaVsAreaData, setPaVsAreaData] = React.useState([])

    useEffect(() => {
        getPatientGroupArea(value,async (res)=>{
            setPaVsAreaData(res)
        })
        getPatientStatus(value,async (res)=>{
            setDieData(res)
        })
        return () => {
            
        }
    }, [value])

    return (
        <Paper style={{ position: "fixed", zIndex: 100, width: "25%", height: "80%", top: 50, left: 10, padding: 20,overflow:"auto" }} >
            <h3>More Infor</h3>
            <Typography component="p" noWrap>
                Name : {Name}
            </Typography>
            <br />

            <TextField
                fullWidth
                id="date"
                label="Birthday"
                type="date"
                onChange={(e)=>handleChange(e.target.value)}
                defaultValue="2017-05-24"
                value={value}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <Grid container
                justifyContent="center"
                alignItems="center">
                <Grid item md={6}>
                    <PieCh data={PaVsAreaData} name={"Số ca theo quận huyện"}/>
                </Grid>
                <Grid item md={6}>
                    <PieCh data={DieData} name={"Tỉ lệ hồi phục"} />

                </Grid>
                <Grid item md={12}>
                    <LineCh />
                </Grid>
            </Grid>
        </Paper>
    )
}

export default React.memo(LeftNav)
