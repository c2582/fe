import { configureStore } from '@reduxjs/toolkit';
import MapStatusReducer from "../feature/MapStatusSlice"

export const store = configureStore({
  reducer: {
    MapStatus : MapStatusReducer
  },
});
