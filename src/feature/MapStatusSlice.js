import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    regionLevel : true,
    districtLevel: false,
    queryDateTime:""
}

export const MapStatusSlice = createSlice({
    name:'MapStatus',
    initialState,
    reducers: {
        setRegionStatus: (state, action)=>{
            state.regionLevel = action.payload
        },
        setDistrictStatus: (state, action)=>{
            state.districtLevel = action.payload
        },
        setqueryDateTime: (state, action)=>{
            state.queryDateTime = action.payload
        },
    }
})

export const {setRegionStatus,setDistrictStatus,setqueryDateTime} = MapStatusSlice.actions
export default MapStatusSlice.reducer