import React, { useEffect } from 'react'
import {
  GoogleMap,
  HeatmapLayer,
  
  Polygon,
  LoadScript,
  Marker,
  Circle
} from '@react-google-maps/api'
import MapsHomeWorkIcon from '@mui/icons-material/MapsHomeWork';
import BenhNhan from "../../config/dnmap/pois.json"
import CustomInforWindow from '../../components/InforWindow';
import { Button } from '@mui/material';
import LeftNav from '../../components/LeftNav';
import DistricDataTest from "../../config/dnmap/districtAreas.json"
import {getPatientStatus,getAllPatient,getRegionStatusMap,getDistrictStatusMap} from "../../api/patientApi"
import {useSelector} from "react-redux"
import Legend from '../../components/Legend';
const libs = ["places"];


const options = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.8,
  strokeWeight: 2,
  fillColor: '#FF0000',
  fillOpacity: 0.5,
  clickable: true,
  draggable: false,
  editable: false,
  visible: true,

  zIndex: 1000
}

function Table() {
  const containerStyle = {
    width: '100%',
    height: '100vh'
  };

  const center = {
    lat: -3.745,
    lng: -38.523
  };
  const onLoad = circle => {
    console.log('Circle onLoad circle: ', circle)
  }

  const onUnmount = circle => {
    console.log('Circle onUnmount circle: ', circle)
  }
  const RegionLevel = useSelector(state => state.MapStatus.regionLevel)
  const DistrictLevel = useSelector(state => state.MapStatus.districtLevel)
  const QueryDateTime = useSelector(state => state.MapStatus.queryDateTime)
  const [OnDisplayInforWindow, setOnDisplayInforWindow] = React.useState(true)
  const [InforWIndowPosition, setInforWIndowPosition] = React.useState({ lat: 16.037833, lng: 108.156697 })
  const [InforWindowContent, setInforWindowContent] = React.useState("Hello")
  const [District, setDistrict] = React.useState([])
  const [Center, setCenter] = React.useState({ lat: 16.037833, lng: 108.156697 })
  const [Detail, setDetail] = React.useState({Name:"Thông tin toàn Đà Nẵng",ares:null})
  const handlePointClick = async (e, id, address, info) => {
    setOnDisplayInforWindow(false)
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();
    console.log(lat, lng, info);
    setInforWindowContent(`${id} \n ${address} \n ${info}`)
    setInforWIndowPosition({ lat: lat, lng: lng })
    setOnDisplayInforWindow(true)
    setCenter({ lat: lat, lng: lng })
  }
  const handleAreaClick = (e,area)=>{
    setDetail({Name:"Thông tin "+area.name,ares:area})
  }
  const handlePolyHover = (e, message)=>{
    setOnDisplayInforWindow(false)
    
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();
    console.log(lat, lng, message);
    setInforWindowContent(`<h3>Khu vuc ${message.name}</h3>
    <p><strong>Số ca ghi nhận:</strong>${message.pa_status.DDT}</p>
    <p><strong>Đã khỏi:</strong>${message.pa_status.KB}</p>
    <p><strong>Tử vong:</strong>${message.pa_status.TV}</p>
    `)
    setInforWIndowPosition({ lat: lat, lng: lng })
    setOnDisplayInforWindow(true)
    // setCenter({ lat: lat, lng: lng })
  }
  const handleDistrictHover = (e, message)=>{
    setOnDisplayInforWindow(false)
    
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();
    console.log(lat, lng, message);
    setInforWindowContent(`<h3>Khu vuc ${message.xa_phuong}</h3>
    <p><strong>Số ca ghi nhận:</strong>${message.pa_status.DDT}</p>
    <p><strong>Đã khỏi:</strong>${message.pa_status.KB}</p>
    <p><strong>Tử vong:</strong>${message.pa_status.TV}</p>
    `)
    setInforWIndowPosition({ lat: lat, lng: lng })
    setOnDisplayInforWindow(true)
    // setCenter({ lat: lat, lng: lng })
  }
  const [Areas, setAreas] = React.useState([])
  useEffect(() => {
    getRegionStatusMap(QueryDateTime,(Poly)=>{
      setAreas(Poly.map((pl) => {
        let tmp = pl;
        try {
          tmp.polygon = JSON.parse(pl.polygon).map((p) => ({ lat: p[0], lng: p[1] }))
          return tmp
        } catch (error) {
          console.log(error);
          return tmp
        }
  
      }));
    })
    
    getDistrictStatusMap(QueryDateTime,(DistinctData)=>{
      setDistrict(DistinctData.map((dis) => {
        let tmp = dis;
        try {
          // console.log("raw",dis.polygon);
          tmp.polygon = JSON.parse(dis.polygon).map((p) => ({ lat: p[0], lng: p[1] }))
          // console.log("pl",tmp.polygon);
          return tmp
        } catch (error) {
          console.log(error);
          tmp.polygon = dis.polygon.map((p) => ({ lat: p[0], lng: p[1] }))
          // return dis
        }
  
      }));
    })
    
    console.log("District state", District);
    return () => {

    }
  }, [QueryDateTime])
  const AreaOption = (color, strokeColor) => {
    console.log(color);
    let options = {
      fillColor: color,
      fillOpacity: 0.8,
      strokeColor: strokeColor,
      strokeOpacity: 0.8,
      strokeWeight: 1,
      clickable: false,
      draggable: false,
      editable: false,
      geodesic: false,
      zIndex: 10
    }
    return options
  }
  const loadDistrict = () => {
    setDistrict(DistricDataTest.map((dis) => {
      let tmp = dis;
      try {
        // console.log("raw",dis.polygon);
        tmp.polygon = JSON.parse(tmp.polygon).map((p) => ({ lat: p[0], lng: p[1] }))
        // console.log("pl",tmp.polygon);
        return tmp
      } catch (error) {
        console.log(error);
        console.log("raw", dis.polygon);
        tmp.polygon = dis.polygon.map((p) => {
          console.log("raw1", p);
          return p
        })
        // console.log("pl",tmp.polygon);
        return tmp
      }

    }));
    console.log("District state", District);
  }
  return (
    <div>
      <LeftNav name={Detail.Name} area={Detail.ares}/>
      <Legend/>
      <LoadScript
        googleMapsApiKey="AIzaSyCuj8kSWmXoQAsxq5Prlt4hyxkuRfDJn00"
        libraries={libs}
      >
        <GoogleMap
          mapContainerStyle={containerStyle}
          center={Center}
          zoom={10}
        >
          <Button onClick={loadDistrict} style={{ position: "absolute", top: 3, right: 3, zIndex: 20 }} variant="contained">
            Load district
          </Button>
          { /* Child components, such as markers, info windows, etc. */}
          <Marker
            position={{ lat: 16.037833, lng: 108.156697 }}
          />
          <Circle
            onLoad={onLoad}
            onUnmount={onUnmount}
            radius={100}
            center={{ lat: 16.037833, lng: 108.156697 }}
            options={options}
          />
          {
            BenhNhan.map((bn) => <Marker icon={{
              url:bn.logo,
              fillOpacity: 0.9,
              
              size: {width: 30, height: 30}, anchor: {x: 0, y: 0}, scaledSize: {width: 20, height: 30},
              strokeColor: "gold",
              strokeWeight: 1}} position={{ lat: bn.lat, lng: bn.lng }} clickable={true} options={options} onClick={(e) => {
              handlePointClick(e, bn.id, bn.address, bn.description)
            }} />)
          }
          {RegionLevel ? 
            Areas.map((pl) => <Polygon clickable={true} paths={pl.polygon}  onClick={(e)=>{handlePolyHover(e,pl)}} options={
              {
                fillColor: pl.color_hex_background,
                fillOpacity: 0.5,
                strokeColor: "black",
                strokeOpacity: 0.8,
                strokeWeight: 1,
                clickable: true,
                draggable: false,
                editable: false,
                geodesic: true,
                zIndex: 10
              }
            } />) : null
          }
          {DistrictLevel ? District.map((dis) =>
          (<Polygon paths={dis.polygon} clickable={true} onClick={(e)=>handleDistrictHover(e,dis)} options={{
            fillColor: dis.ma_mau,
            fillOpacity: 0.35,
            strokeColor: "black",
            strokeOpacity: 0.8,
            strokeWeight: 1,
            clickable: true,
            draggable: false,
            editable: false,
            geodesic: true,
            zIndex: 100
          }} />)

          ): null

          }
          {OnDisplayInforWindow && (
            <CustomInforWindow
              position={InforWIndowPosition}
              content={InforWindowContent} />
          )}

        </GoogleMap>
      </LoadScript>
    </div>
  )
}




export default Table
