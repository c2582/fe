FROM nginx
WORKDIR /app
COPY build .
COPY nginx/apin.conf /etc/nginx/conf.d/apin.conf
EXPOSE 3000
# CMD ["nginx"]
